!    y=1 ______________                                                                                 
!       /             /|       |  Author  : Zi-Hsuan Wei                                                 
!      /             / |       |  Version : 1.8                                                          
!     /____________ /  |       |  Web     : http://smetana.me.ntust.edu.tw/                              
!     |  |         |   |                                                        
!     |  |         |   |                                          
!     |  | x=y=z=0 |   |                                           
!     |  |_________|___|x=1                                        
!     |  /         |  /                                         
!     | /          | /                                        
!     |/___________|/                                         
!    z=1                                                  
!                                                       
program main
   use variables
   use mpi
   use omp_lib
   implicit none

   

   !------------------- OPENMP ------------------------!
   nthreads = 5
   call omp_set_num_threads(nthreads)
   !------------------- OPENMP ------------------------!

   !------------------------ MPI ------------------------!
   call MPI_INIT(ierr)
   call MPI_COMM_SIZE(MPI_COMM_WORLD, nproc, ierr)
   call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
   call Mpi_division()
   !------------------------ MPI ------------------------!


   !---------for nz----------!
   i_jpstart = jpstart(mycid)!
   i_jpend   = jpend(mycid)  !
   !---------for nz----------!

   !---------for ny----------!
   i_ipstart = ipstart(mycid)!
   i_ipend   = ipend(mycid)  !
   !---------for ny----------!


   
   !-----------------Parameters for the simulation------------------!
   
   omega                          = 1.6               ! Set value for SOR method

   zeta                           = 1.e-4             ! zeta for solving pressure matrix

   itmax                          = 3000              ! maximum for zeta in Gauss Seidel subroutines

   zeta_vel                       = 1.e-10            ! convergence condition for velocity field

   time                           = 0.0               ! initialize time of simulation
      
   nstep                          = 100000            ! number of timesteps for the simulation
   
   isto                           = 100               ! data stored every 'isto' steps
   
   LES                            = 1                 ! the LES mode. 1 : on ; 0 : off
   
   steadiness                     = 2                 ! steady : 1 ; unsteady : 2 
   
   totalcosttime                  = 0                 ! initialize wall time
   
   inputfile                      = '0000.Q'          !input first q file
   
   
   !-----------------Parameters for the simulation------------------!



   !--------------------------------------------------------------------!
   call reading_data()              ! define dx, dy, dz, nu = 1./Re
   !--------------------------------------------------------------------!

   
   !--------------------------------------------------------------------!
   if(Gridder=='non-uniform')then
      call gridder_unequal()           ! define unequal mesh
   else if(Gridder=='uniform')then
      call gridder_equal()             ! define equal mesh
   end if
   !--------------------------------------------------------------------!

   !--------------------------------------------------------------------!
   call initial_conditions()        !call initial conditions
   !--------------------------------------------------------------------!

   !--------------------------------------------------------------------!
   !call reading_variables()        !input first q file
   !--------------------------------------------------------------------!

   !--------------------------------------------------------------------!
   call boundary_conditions()       !call boundary conditions
   !--------------------------------------------------------------------!
   

   !--------------------------------------------------------------------! 
   !call DFIB_Sphere()      
   !--------------------------------------------------------------------!


   

   if(mycid==master)then

      call filereachtime()     
      call filerInfo()

   end if

   !----------------------------for sendrecv-----------------------------!
   if( L_nbr == -1 )then; L_nbr = MPI_PROC_NULL; end if                  !
   if( R_nbr == -1 )then; R_nbr = MPI_PROC_NULL; end if                  !
   if( B_nbr == -1 )then; B_nbr = MPI_PROC_NULL; end if                  !
   if( T_nbr == -1 )then; T_nbr = MPI_PROC_NULL; end if                  !
   !----------------------------for sendrecv-----------------------------!

   !call MPI_BARRIER(comm2D, ierr)

   !write(*,*) mycid, L_nbr, R_nbr, B_nbr, T_nbr

   call MPI_BARRIER(comm2D, ierr)

   write(*,*) mycid, i_jpstart, i_jpend, &   ! k-direction
                     i_ipstart, i_ipend      ! j-direction

   call MPI_BARRIER(comm2D, ierr)

!-------------------------main loop on the timesteps----------------------!
   do istep=1,nstep                                                                      
      totalstarttime = MPI_WTIME()

   
      
      !----------data transformation among nodes----------! nz*nx(non-continue) B_nbr, T_nbr
      icount = ((i_jpend-i_jpstart)+1)

      call MPI_TYPE_VECTOR(icount, nx+4, (ny+4)*(nx+4), MPI_REAL8, VECT_nznx, ierr) 
      call MPI_TYPE_COMMIT(VECT_nznx, ierr) 

      itag = 110 !
      call MPI_SENDRECV( u(-1,i_ipend-1,i_jpstart), 1, VECT_nznx, T_nbr, itag, &
                         u(-1,i_ipstart-2,i_jpstart), 1, VECT_nznx, B_nbr, itag, comm2D, status, ierr )
      itag = 120
      call MPI_SENDRECV( u(-1,i_ipend,i_jpstart), 1, VECT_nznx, T_nbr, itag, &
                         u(-1,i_ipstart-1,i_jpstart), 1, VECT_nznx, B_nbr, itag, comm2D, status, ierr )

      itag = 130 !
      call MPI_SENDRECV( v(-1,i_ipend-1,i_jpstart), 1, VECT_nznx, T_nbr, itag, &
                         v(-1,i_ipstart-2,i_jpstart), 1, VECT_nznx, B_nbr, itag, comm2D, status, ierr )
      itag = 140
      call MPI_SENDRECV( v(-1,i_ipend,i_jpstart), 1, VECT_nznx, T_nbr, itag, &
                         v(-1,i_ipstart-1,i_jpstart), 1, VECT_nznx, B_nbr, itag, comm2D, status, ierr )

      itag = 150 !
      call MPI_SENDRECV( w(-1,i_ipend-1,i_jpstart), 1, VECT_nznx, T_nbr, itag, &
                         w(-1,i_ipstart-2,i_jpstart), 1, VECT_nznx, B_nbr, itag, comm2D, status, ierr )
      itag = 160
      call MPI_SENDRECV( w(-1,i_ipend,i_jpstart), 1, VECT_nznx, T_nbr, itag, &
                         w(-1,i_ipstart-1,i_jpstart), 1, VECT_nznx, B_nbr, itag, comm2D, status, ierr )




      itag = 170 !
      call MPI_SENDRECV( u(-1,i_ipstart,i_jpstart), 1, VECT_nznx, B_nbr, itag, &
                         u(-1,i_ipend+1,i_jpstart), 1, VECT_nznx, T_nbr, itag, comm2D, status, ierr )
      itag = 180
      call MPI_SENDRECV( u(-1,i_ipstart+1,i_jpstart), 1, VECT_nznx, B_nbr, itag, &
                         u(-1,i_ipend+2,i_jpstart), 1, VECT_nznx, T_nbr, itag, comm2D, status, ierr )

      itag = 190 !
      call MPI_SENDRECV( v(-1,i_ipstart,i_jpstart), 1, VECT_nznx, B_nbr, itag, &
                         v(-1,i_ipend+1,i_jpstart), 1, VECT_nznx, T_nbr, itag, comm2D, status, ierr )
      itag = 200
      call MPI_SENDRECV( v(-1,i_ipstart+1,i_jpstart), 1, VECT_nznx, B_nbr, itag, &
                         v(-1,i_ipend+2,i_jpstart), 1, VECT_nznx, T_nbr, itag, comm2D, status, ierr )

      itag = 210 !
      call MPI_SENDRECV( w(-1,i_ipstart,i_jpstart), 1, VECT_nznx, B_nbr, itag, &
                         w(-1,i_ipend+1,i_jpstart), 1, VECT_nznx, T_nbr, itag, comm2D, status, ierr )
      itag = 220
      call MPI_SENDRECV( w(-1,i_ipstart+1,i_jpstart), 1, VECT_nznx, B_nbr, itag, &
                         w(-1,i_ipend+2,i_jpstart), 1, VECT_nznx, T_nbr, itag, comm2D, status, ierr )

      call MPI_BARRIER(comm2D, ierr)
      !----------data transformation among nodes----------! nz*nx(non-continue) B_nbr, T_nbr


      !----------data transformation among nodes----------! ny*nx(continue) R_nbr, L_nbr
      icount = (nx+4)*((i_ipend-i_ipstart)+1)    
      ipos = 0

      !call MPI_PACK(u(-1,i_ipstart,i_jpend-1), icount, MPI_REAL8, bufRL1, bufSize, ipos)

      itag = 230 
      call MPI_SENDRECV( u(-1,i_ipstart,i_jpend-1), icount, MPI_REAL8, r_nbr, itag, &
                         u(-1,i_ipstart,i_jpstart-2), icount, MPI_REAL8, l_nbr, itag, comm2D, status, ierr )
      itag = 240 !
      call MPI_SENDRECV( u(-1,i_ipstart,i_jpend), icount, MPI_REAL8, r_nbr, itag, &
                         u(-1,i_ipstart,i_jpstart-1), icount, MPI_REAL8, l_nbr, itag, comm2D, status, ierr )

      itag = 250 
      call MPI_SENDRECV( v(-1,i_ipstart,i_jpend-1), icount, MPI_REAL8, r_nbr, itag, &
                         v(-1,i_ipstart,i_jpstart-2), icount, MPI_REAL8, l_nbr, itag, comm2D, status, ierr )
      itag = 260 !
      call MPI_SENDRECV( v(-1,i_ipstart,i_jpend), icount, MPI_REAL8, r_nbr, itag, &
                         v(-1,i_ipstart,i_jpstart-1), icount, MPI_REAL8, l_nbr, itag, comm2D, status, ierr )

      itag = 270 
      call MPI_SENDRECV( w(-1,i_ipstart,i_jpend-1), icount, MPI_REAL8, r_nbr, itag, &
                         w(-1,i_ipstart,i_jpstart-2), icount, MPI_REAL8, l_nbr, itag, comm2D, status, ierr )
      itag = 280 !
      call MPI_SENDRECV( w(-1,i_ipstart,i_jpend), icount, MPI_REAL8, r_nbr, itag, &
                         w(-1,i_ipstart,i_jpstart-1), icount, MPI_REAL8, l_nbr, itag, comm2D, status, ierr )





      itag = 290
      call MPI_SENDRECV( u(-1,i_ipstart,i_jpstart), icount, MPI_REAL8, l_nbr, itag, &
                         u(-1,i_ipstart,i_jpend+1), icount, MPI_REAL8, r_nbr, itag, comm2D, status, ierr )
      itag = 300
      call MPI_SENDRECV( u(-1,i_ipstart,i_jpstart+1), icount, MPI_REAL8, l_nbr, itag, &
                         u(-1,i_ipstart,i_jpend+2), icount, MPI_REAL8, r_nbr, itag, comm2D, status, ierr )

      itag = 310
      call MPI_SENDRECV( v(-1,i_ipstart,i_jpstart), icount, MPI_REAL8, l_nbr, itag, &
                         v(-1,i_ipstart,i_jpend+1), icount, MPI_REAL8, r_nbr, itag, comm2D, status, ierr )
      itag = 320
      call MPI_SENDRECV( v(-1,i_ipstart,i_jpstart+1), icount, MPI_REAL8, l_nbr, itag, &
                         v(-1,i_ipstart,i_jpend+2), icount, MPI_REAL8, r_nbr, itag, comm2D, status, ierr )

      itag = 330
      call MPI_SENDRECV( w(-1,i_ipstart,i_jpstart), icount, MPI_REAL8, l_nbr, itag, &
                         w(-1,i_ipstart,i_jpend+1), icount, MPI_REAL8, r_nbr, itag, comm2D, status, ierr )
      itag = 340
      call MPI_SENDRECV( w(-1,i_ipstart,i_jpstart+1), icount, MPI_REAL8, l_nbr, itag, &
                         w(-1,i_ipstart,i_jpend+2), icount, MPI_REAL8, r_nbr, itag, comm2D, status, ierr )

      call MPI_BARRIER(comm2D, ierr)
      !----------data transformation among nodes----------! ny*nx(continue) R_nbr, L_nbr


      !------------------------------------------------------------------------------------------------------!
      call CalculateSmagorinskyViscosity()    ! calculate Smagorinsky Viscosity
      !------------------------------------------------------------------------------------------------------!
 
      !------------------------------------------------------------------------------------------------------!
      call discretisation_QUICK_centre()      ! calculate velocity field
      !------------------------------------------------------------------------------------------------------!


      !----------data transformation among nodes----------! w_star
      icount = (nx+4)*((i_ipend-i_ipstart)+1)  
      itag = 350
      call MPI_SENDRECV( w_star(-1,i_ipstart,i_jpend), icount, MPI_REAL8, r_nbr, itag, &
                         w_star(-1,i_ipstart,i_jpstart-1), icount, MPI_REAL8, l_nbr, itag, comm2D, status, ierr )
      
      call MPI_BARRIER(comm2D, ierr)
      !----------data transformation among nodes----------!
      
      !----------data transformation among nodes----------! v_star
      icount = ((i_jpend-i_jpstart)+1)

      call MPI_TYPE_VECTOR(icount, nx+4, (ny+4)*(nx+4), MPI_REAL8, VECT_nznx, ierr) 
      call MPI_TYPE_COMMIT(VECT_nznx, ierr) 

      itag = 360
      call MPI_SENDRECV( v(-1,i_ipend,i_jpstart), 1, VECT_nznx, T_nbr, itag, &
                         v(-1,i_ipstart-1,i_jpstart), 1, VECT_nznx, B_nbr, itag, comm2D, status, ierr )

      call MPI_BARRIER(comm2D, ierr)
      !----------data transformation among nodes----------!


      !------------------------------------------------------------------------------------------------------!
      call gauss_seidel()       ! calculate pressure field
      !------------------------------------------------------------------------------------------------------!
      

      !----------data transformation among nodes----------! nz*nx(non-continue) B_nbr, T_nbr
      icount = ((i_jpend-i_jpstart)+1)

      call MPI_TYPE_VECTOR(icount, nx+4, (ny+4)*(nx+4), MPI_REAL8, VECT_nznx, ierr) 
      call MPI_TYPE_COMMIT(VECT_nznx, ierr) 

      itag = 410
      call MPI_SENDRECV( p(-1,i_ipstart,i_jpstart), 1, VECT_nznx, B_nbr, itag, &
                         p(-1,i_ipend+1,i_jpstart), 1, VECT_nznx, T_nbr, itag, comm2D, status, ierr )
      
      call MPI_BARRIER(comm2D, ierr)
      !----------data transformation among nodes----------! nz*nx(non-continue) B_nbr, T_nbr



      !----------data transformation among nodes----------! ny*nx(continue) R_nbr, L_nbr
      icount = (nx+4)*((i_ipend-i_ipstart)+1)  

      itag = 420
      call MPI_SENDRECV( p(-1,i_ipstart,i_jpstart), icount, MPI_REAL8, l_nbr, itag, &
                         p(-1,i_ipstart,i_jpend+1), icount, MPI_REAL8, r_nbr, itag, comm2D, status, ierr )
      !----------data transformation among nodes----------! ny*nx(continue) R_nbr, L_nbr


      !------------------------------------------------------------------------------------------------------!
      call calcul_new_velocity() ! update velocity field 
      !------------------------------------------------------------------------------------------------------!


      !------------------------------------------------------------------------------------------------------!
      call virtualForceIntegrator() ! COEFFICIENTS: CD and Center_CL,Distance,Velocity for solid  
      !------------------------------------------------------------------------------------------------------!

      !------------------------------------------------------------------------------------------------------!
      call Updating_velocity()   ! update velocity field 
      !------------------------------------------------------------------------------------------------------!
      

      !------------------------------------------------------------------------------------------------------!
      call boundary_conditions() ! recall boundary conditions to update them
      !------------------------------------------------------------------------------------------------------!
      
      









      time = time + dt
      !----------data collect among nodes----------!
      if (mod(istep,isto)==0) then ! write results if isto = istep

            icount = ((i_jpend-i_jpstart)+1)

            call MPI_TYPE_VECTOR(icount, (nx)*((i_ipend-i_ipstart)+1), (ny)*(nx), MPI_REAL8, VECT_nznynx, ierr) 
            call MPI_TYPE_COMMIT(VECT_nznynx, ierr) 


            !Send my results back to the master
            if(mycid>master)then

               
               itag = 10
               call MPI_SEND( pre(1,i_ipstart,i_jpstart), 1, VECT_nznynx, master, itag, comm2D, ierr )

               itag = 20
               call MPI_SEND( uc(1,i_ipstart,i_jpstart), 1, VECT_nznynx, master, itag, comm2D, ierr )

               itag = 30
               call MPI_SEND( vc(1,i_ipstart,i_jpstart), 1, VECT_nznynx, master, itag, comm2D, ierr )

               itag = 40
               call MPI_SEND( wc(1,i_ipstart,i_jpstart), 1, VECT_nznynx, master, itag, comm2D, ierr )

            end if


            !Wait to receive results from each task
            if(mycid==master)then

               do i = 1, (nproc-1)

                        itag = 10
                        call MPI_RECV( pre(1,ipstart(i),jpstart(i)), 1, VECT_nznynx, i, itag, comm2D, status, ierr )

                        itag = 20
                        call MPI_RECV( uc(1,ipstart(i),jpstart(i)), 1, VECT_nznynx, i, itag, comm2D, status, ierr )

                        itag = 30
                        call MPI_RECV( vc(1,ipstart(i),jpstart(i)), 1, VECT_nznynx, i, itag, comm2D, status, ierr )

                        itag = 40
                        call MPI_RECV( wc(1,ipstart(i),jpstart(i)), 1, VECT_nznynx, i, itag, comm2D, status, ierr )


               end do


            end if
            
      end if
      !----------data collect among nodes----------!


      if(mycid==master)then

         write(*,*) 'time = ',time  
         call filerProcess()
               
      end if
      

       



      VelocityDifference = 1024
      if (mod(istep,isto)==0) then ! write results if isto = istep
               
         if(mycid==master)then

            call filereachtime()
            
         end if 

         call check_steady()

      end if

           








      !----------calculate wall time----------!
      totalfinaltime = MPI_WTIME()
      totalcosttime = totalcosttime + (totalfinaltime-totalstarttime)
      if(mycid==master)then 
         write(*,*) 'each cost = ', totalfinaltime-totalstarttime, 'total cost = ', totalcosttime
      end if
      !----------calculate wall time----------!




      
      

   end do
   istep = istep - 1
!-------------------------main loop on the timesteps----------------------!


   if(mycid==master)then; call filer_final(); end if

   call MPI_FINALIZE(ierr) 

end program main